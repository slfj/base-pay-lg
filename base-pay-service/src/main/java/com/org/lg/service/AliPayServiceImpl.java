package com.org.lg.service;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.stereotype.Service;

import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.AlipayTradePagePayModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.jpay.alipay.AliPayApi;
import com.jpay.util.StringUtils;
import com.org.lg.api.AliPayService;
import com.org.lg.bean.PayConfig;
import com.org.lg.constant.Constant;
import com.org.lg.model.AliApiResult;
import com.org.lg.model.TradePagePayModel;
import com.org.lg.model.TradeRefundModel;
import com.org.lg.model.TradeWapPayModel;

import lombok.extern.java.Log;
@Log
@Service
public class AliPayServiceImpl implements AliPayService {
	
	@Override
	public void pcPay(HttpServletRequest request,HttpServletResponse response,TradePagePayModel payModel) {
		try {
			AlipayTradePagePayModel model = new AlipayTradePagePayModel();
			PropertyUtils.copyProperties(model, payModel);
			model.setProductCode(Constant.FAST_INSTANT_TRADE_PAY);
			if(payModel==null) {
				throw new AlipayApiException(AliApiResult.get(-1));
			}
			if(StringUtils.isBlank(payModel.getNotifyUrl())||StringUtils.isBlank(payModel.getReturnUrl())) {
				payModel.setNotifyUrl(PayConfig.AliPay.domain+Constant.NOTIFY_ALI_NOTIFY_URL);
				payModel.setReturnUrl(PayConfig.AliPay.domain+Constant.NOTIFY_ALI_RETURN_URL);
			}
			//花呗分期相关的设置
			/**
			 * 测试环境不支持花呗分期的测试
			 * hb_fq_num代表花呗分期数，仅支持传入3、6、12，其他期数暂不支持，传入会报错；
			 * hb_fq_seller_percent代表卖家承担收费比例，商家承担手续费传入100，用户承担手续费传入0，仅支持传入100、0两种，其他比例暂不支持，传入会报错。
			 */
			//ExtendParams extendParams = new ExtendParams();
			//extendParams.setHbFqNum("3");
			//extendParams.setHbFqSellerPercent("0");
			//model.setExtendParams(extendParams);
			AliPayApi.tradePage(response,model , payModel.getNotifyUrl(), payModel.getReturnUrl());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean rsaCheckV1(Map<String, String> map) throws AlipayApiException {
		return AlipaySignature.rsaCheckV1(map, PayConfig.AliPay.alipayPublicKey, "UTF-8",PayConfig.AliPay.signType);
	}

	@Override
	public String tradeRefund(TradeRefundModel tradeRefundModel) throws AlipayApiException {
		AlipayTradeRefundModel model = new AlipayTradeRefundModel();
		try {
			PropertyUtils.copyProperties(model, tradeRefundModel);
			return AliPayApi.tradeRefund(model);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException |AlipayApiException e) {
			e.printStackTrace();
			throw new AlipayApiException(AliApiResult.get(-1));
		}
	}

	@Override
	public void wapPay(HttpServletResponse response, TradeWapPayModel payModel) throws AlipayApiException{
		if(payModel==null) {
			throw new AlipayApiException(AliApiResult.get(-1));
		}
		AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
		try {
			PropertyUtils.copyProperties(model, payModel);
			if(StringUtils.isBlank(payModel.getNotifyUrl())||StringUtils.isBlank(payModel.getReturnUrl())) {
				payModel.setNotifyUrl(PayConfig.AliPay.domain+Constant.NOTIFY_ALI_NOTIFY_URL);
				payModel.setReturnUrl(PayConfig.AliPay.domain+Constant.NOTIFY_ALI_RETURN_URL);
			}
			model.setProductCode(Constant.QUICK_WAP_PAY);
			AliPayApi.wapPay(response, model, payModel.getNotifyUrl(), payModel.getReturnUrl());
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException  |AlipayApiException| IOException e) {
			e.printStackTrace();
			log.warning(e.getMessage());
			throw new AlipayApiException(AliApiResult.get(-1));
		}
	}

}
