package com.org.lg.wechat.controller;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.support.json.JSONUtils;
import com.org.lg.annotation.Clear;
import com.org.lg.wechat.config.WxMpConfiguration;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

@RestController
@RequestMapping("/wx/oauth")
public class WxOauthController extends WxBaseController{
	
	  @Clear
	  @RequestMapping("/index")
	  public WxMpUser index(HttpServletRequest request,HttpServletResponse response,String code,String state) throws WxErrorException, IOException{
		  System.out.println("code ::::::::::::::::::::::::"+code);
		  WxMpService wxMpService = WxMpConfiguration.getMpServices().get("wxb0fdd232d08d8dca");
		  WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
		  boolean valid = wxMpService.oauth2validateAccessToken(wxMpOAuth2AccessToken);
		  if(!valid){
			  wxMpOAuth2AccessToken = wxMpService.oauth2refreshAccessToken(wxMpOAuth2AccessToken.getRefreshToken());
		  }
		  WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);
		  System.out.println("-----------------------------获取当前用户信息----------------------------");
		  System.out.println(wxMpUser.getNickname());
		  return wxMpUser;
	  }
	  
	  
	  @RequestMapping("/wxvue")
	  public String wxvue() {
		  
		  return "test";
	  }
}
