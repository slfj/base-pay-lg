package com.org.lg.wechat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import com.jpay.util.StringUtils;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class JRedisConfig {

	
	  @Value("${spring.redis.host}")
	  private String host;

	  @Value("${spring.redis.port}")
	  private int port;

	  @Value("${spring.redis.timeout}")
	  private int timeout;

	  @Value("${spring.redis.password}")
	  private String password;

	  @Value("${spring.redis.database}")
	  private int database;

	  @Value("${spring.redis.jedis.pool.max-idle}")
	  private int maxIdle;

	  @Value("${spring.redis.jedis.pool.min-idle}") 
	  private int minIdle;
	
	@Bean
    public JedisPool jedisPool() {
		JedisPool jedisPool = new JedisPool(jedisPoolConfig(), host, port, timeout ,null,database);
        if(StringUtils.isNotEmpty(password)){
        	jedisPool = new JedisPool(jedisPoolConfig(), host, port, timeout,password,database);
        }
        return jedisPool;
    }
    
    @Bean
    JedisPoolConfig jedisPoolConfig(){
	    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMaxWaitMillis(2);
        return jedisPoolConfig;
    }
    
    @SuppressWarnings("deprecation")
	@Bean  
    public JedisConnectionFactory getConnectionFactory(){  
        JedisConnectionFactory factory = new JedisConnectionFactory();  
        factory.setHostName(host);
        factory.setPort(port);
        factory.setTimeout(timeout);
        factory.setPassword(password);
        factory.setDatabase(1);
        factory.setPoolConfig(jedisPoolConfig());
        return factory;
    }
}
