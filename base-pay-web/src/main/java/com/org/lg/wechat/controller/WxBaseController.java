package com.org.lg.wechat.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.org.lg.bean.PayConfig;
import com.org.lg.wechat.config.WxMpConfiguration;

import me.chanjar.weixin.mp.api.WxMpService;

@Component
@DependsOn(value="wxpay")
public class WxBaseController {
	
	@Bean
	WxMpService wxMpService() {
		 WxMpService wxMpService = WxMpConfiguration.getMpServices().get(PayConfig.WxPay.appId);
		 return wxMpService;
	}
	
}
