package com.org.lg.controller;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jpay.ext.kit.IpKit;
import com.jpay.ext.kit.StrKit;
import com.org.lg.api.WxPayService;
import com.org.lg.model.H5PayModel;
import com.org.lg.model.WechatPayModel;
import com.org.lg.model.WxApiResult;
import com.org.lg.model.WxTradeRefundModel;

/**
 * @author LG
 * @version 1.0.0
 * @date 2018-10-31 10:26
 */
@RestController
@RequestMapping("wxpay")
public class WxPayController {
	
	@Autowired
	WxPayService wxPayService;
	
	/**微信扫码支付，传过来订单号和订单类型,根据对应的订单号和订单类型查询相应的订单详情来获取订单支付的二维码用于扫码支付
	 * @param orderNo
	 * @param orderType
	 */
	@RequestMapping("scanPay")
	public WxApiResult scanPay(@PathVariable("orderNo") String orderNo,@PathVariable("orderType") String orderType){
		Map<String, String> param=new HashMap<>();
		WxApiResult res=wxPayService.scanPay(param);
		return res;
	}
	
	
	/**h5支付唤醒手机app
	 * @param request
	 * @param response
	 */
	@RequestMapping("h5Pay")
	public WxApiResult h5Pay(HttpServletRequest request,HttpServletResponse response){
		String ip = IpKit.getRealIp(request);
		H5PayModel h5Model=new H5PayModel();
		h5Model.setIp(ip);
		return wxPayService.h5Pay(response, h5Model);
	}
	
	
	
	/**
	 * 公众号支付
	 */
	@RequestMapping(value ="/wechatPay",method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public WxApiResult webPay(HttpServletRequest request,HttpServletResponse response) {
		// openId，采用 网页授权获取 access_token API：SnsAccessTokenApi获取
		String openId = (String) request.getSession().getAttribute("openId");
		String ip = IpKit.getRealIp(request);
		if (StrKit.isBlank(ip)) {
			ip = "127.0.0.1";
		}
		WechatPayModel wechatPayModel=new WechatPayModel();
		wechatPayModel.setOpenId(openId);
		wechatPayModel.setIp(ip);
		wechatPayModel.setTotalFee("0.22");
		wechatPayModel.setOutTradeNo("1i092093203");
		return wxPayService.wechatPay(wechatPayModel);
	}
	
	
	/**
	 * 微信退款
	 */
	@RequestMapping(value = "/refund",method={RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public WxApiResult refund(@RequestParam("transactionId") String transactionId,@RequestParam("out_trade_no") String out_trade_no) {
		WxTradeRefundModel wxtradeRefundModel=new WxTradeRefundModel();
		return wxPayService.tradeRefund(wxtradeRefundModel);
	}
}
