package com.org.lg.controller;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alipay.api.AlipayApiException;
import com.jpay.alipay.AliPayApi;
import com.jpay.util.StringUtils;
import com.org.lg.api.AliPayService;
import com.org.lg.model.TradePagePayModel;
import com.org.lg.model.TradeRefundModel;
import com.org.lg.model.TradeWapPayModel;
/**
 * @author dl
 * @version 1.0.0
 * @date 2018-10-31 10:26
 */
@RestController
@RequestMapping("/pay/alipay")
public class AliPayController {

	
	@Autowired
	AliPayService aliPayService;
	
	
	/**
	 * PC支付
	 */
	@RequestMapping(value = "/pcPay") 
	@ResponseBody
	public void pcPay(HttpServletRequest request,HttpServletResponse response){
		try {
			String totalAmount = "88.88"; 
			String outTradeNo =StringUtils.getOutTradeNo();
			TradePagePayModel payModel=new TradePagePayModel();
			//String returnUrl = "http://154jg07681.iask.in:31343/pay/alipay/return_url";
			//String notifyUrl ="http://154jg07681.iask.in:31343/pay/alipay/notify_url";
			//payModel.setNotifyUrl(notifyUrl);
			//payModel.setReturnUrl(returnUrl);
			payModel.setOutTradeNo(outTradeNo);
			payModel.setTotalAmount(totalAmount);
			payModel.setSubject("PC支付测试");
			payModel.setBody("PC支付测试");
			aliPayService.pcPay(request,response, payModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 退款
	 */
	@RequestMapping(value = "/tradeRefund")
	@ResponseBody
	public String tradeRefund() {
		try {
			TradeRefundModel model=new TradeRefundModel();
			model.setOutTradeNo("081014283315023");
			model.setTradeNo("2017081021001004200200273870");
			model.setRefundAmount("86.00");
			model.setRefundReason("正常退款");
			return aliPayService.tradeRefund(model);
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		return "fail";
	}
	
	
	@RequestMapping(value = "/wapPay") 
	@ResponseBody
	public void wapPay(HttpServletResponse response) {
		String body = "我是测试数据";
		String subject = " Wap支付测试";
		String totalAmount = "1";
		String passbackParams = "1";
		
		TradeWapPayModel model=new TradeWapPayModel();
		model.setBody(body);
		model.setSubject(subject);
		model.setTotalAmount(totalAmount);
		model.setPassbackParams(passbackParams);
		String outTradeNo = StringUtils.getOutTradeNo();
		System.out.println("wap outTradeNo>"+outTradeNo);
		model.setOutTradeNo(outTradeNo);
		try {
			aliPayService.wapPay(response, model);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
