package com.org.lg.controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RestController;
import com.org.lg.base.WxNotifyController;
/**
 * @author LG
 */
@RestController
public class WxNotifyApiController extends WxNotifyController{
	
	public String notifyUrl(HttpServletRequest request) {
		        // 支付结果通用通知文档: https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_7
                 if(verifyNotify(request)) {
                	 //进行业务逻辑处理
                	 return OK();
                 }
                 else {
                	 return "";
                 }
	 }

}
