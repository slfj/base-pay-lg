package com.org.lg.controller;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RestController;

import com.org.lg.base.AliNotifyController;
/**
 * @author dl
 * @version 1.0.0
 * @date 2018-10-31 10:26
 */
@RestController
public class AliNotifyApiController extends AliNotifyController{
	
	public String returnUrl(HttpServletRequest request) {
		try {
			if (rsaCheckV1(request)) {// 验证成功
				// TODO 请在这里加上商户的业务逻辑程序代码
				System.out.println("return_url 验证成功");
				return "success";
			} else {
				System.out.println("return_url 验证失败");
				// TODO
				return "failure";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}


	public String  notifyUrl(HttpServletRequest request) {
		try {
			if (rsaCheckV1(request)) {// 验证成功
				// TODO 请在这里加上商户的业务逻辑程序代码 异步通知可能出现订单重复通知 需要做去重处理
				System.out.println("notify_url 验证成功succcess");
				return "success";
			} else {
				System.out.println("notify_url 验证失败");
				// TODO
				return "failure";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}

}
