package com.org.lg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@ComponentScan(value="com.org.lg.**")
public class BasketCloudApplication {
    public static void main(String[] args) {
        SpringApplication.run(BasketCloudApplication.class, args);
    }
}
