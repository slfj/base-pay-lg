package com.org.lg.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.org.lg.interceptor.WxAuthenInterceptor;

@Configuration
public class WechatMvcConfigurerAdapter extends WebMvcConfigurerAdapter{

	@Bean
	WxAuthenInterceptor wxAuthenInterceptor() {
         return new WxAuthenInterceptor();
    }
	
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
		List<String> paths=new ArrayList<>();
		paths.add("/css/**");
		paths.add("/js/**");
		paths.add("/fonts/**");
		paths.add("/font/**");
		paths.add("/images/**");
		paths.add("/docs/**");
        registry.addInterceptor(wxAuthenInterceptor()).addPathPatterns("/wx/**").excludePathPatterns(paths);//添加wx全局拦截器
    }
}
