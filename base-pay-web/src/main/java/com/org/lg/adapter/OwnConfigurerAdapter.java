package com.org.lg.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Configuration;

@Configuration
public class OwnConfigurerAdapter extends PayWebMvcConfigurerAdapter {

	@Override
	public List<String> wxPayUrl() {
		List<String> wxPath=new ArrayList<>();
		wxPath.add("/wxpay1/**");
		return wxPath;
	}

	@Override
	public List<String> aliPayUrl() {
		List<String> aliPath=new ArrayList<>();
		aliPath.add("/alipay/**");
		return aliPath;
	}

	@Override
	protected List<String> unionPayUrl() {
		List<String> unionPath=new ArrayList<>();
		unionPath.add("/unionpay/**");
		return unionPath;
	}

}

