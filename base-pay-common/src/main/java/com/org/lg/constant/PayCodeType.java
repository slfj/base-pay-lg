package com.org.lg.constant;

public enum PayCodeType {
	SCAN_SUCCESS(0, "生产支付码成功!"),
	SCAN_FAIL(1,"生产支付码失败!"),
    ;

    private int code;
    private String msg;

    PayCodeType(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
