package com.org.lg.constant;

public class Constant {

	
	public static String RETURN_CODE="return_code";
	
	public static String RESULT_CODE="result_code";
	
	public static String RETURN_MSG="return_msg";
	
	public static String CODE_URL="code_url";
	
	public static String PREPAY_ID="prepay_id";
	
	public static String MWEB_URL="mweb_url";
	
	public static String WAP="Wap";
	
	public static String SUCCESS="SUCCESS"; 
	
	
	//交易类型
	public static final String FAST_INSTANT_TRADE_PAY="FAST_INSTANT_TRADE_PAY";
	
	public static final String QUICK_WAP_PAY="QUICK_WAP_PAY"; 
	
	//公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数。支付宝只会在异步通知时将该参数原样返回。本参数必须进行UrlEncode之后才可以发送给支付宝
	public static String PASSBACK_PARAMS="callback params";
	
	
	public static String RES="res";
	
	public static String CODE="code";
	
	public static String MSG="msg";
	
	//notify
	public static final String NOTIFY_ALI= "/notify/alipay";
	
	public static final String NOTIFY_ALI_NOTIFY_URL= NOTIFY_ALI+"/notify_url";
	
	public static final String NOTIFY_ALI_RETURN_URL= NOTIFY_ALI+"/return_url";
	
	public static final String NOTIFY_WX= "/notify/wxpay";
	
	public static final String NOTIFY_WX_NOTIFY_URL= NOTIFY_WX+"/notify_url";
	
}
