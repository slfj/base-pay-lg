package com.org.lg.model;

import com.alipay.api.domain.AlipayTradeWapPayModel;

public class TradeWapPayModel extends AlipayTradeWapPayModel{

    public String notifyUrl;//异步回调地址
	
	public String returnUrl;//同步回调地址

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	
}
