package com.org.lg.model;

public class WxTradeRefundModel extends WechatPayModel{

	private String nonceStr;
	
	private String refundFee;

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getRefundFee() {
		return refundFee;
	}

	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}
	
	
	
}
