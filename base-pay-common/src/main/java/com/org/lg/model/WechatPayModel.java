package com.org.lg.model;

import lombok.Data;

/**
 * @author LG
 *微信支付参数模板
 */
@Data
public class WechatPayModel {

	private String appId;
	
	
	private String mchId;
	
	
	private String openId;
	
	
	private String totalFee;
	
	
	private String ip;
	
	
	private String body;
	
	
	private String attach;
	
	
	private String outTradeNo;
	
	
	private String transactionId;
	
	
	private String productId;
	
	
	private String notifyUrl;
}
