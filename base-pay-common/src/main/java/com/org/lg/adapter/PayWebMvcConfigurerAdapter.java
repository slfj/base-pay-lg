package com.org.lg.adapter;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.org.lg.interceptor.AliPayInterceptor;
import com.org.lg.interceptor.CharacterEncodInterceptor;
import com.org.lg.interceptor.WxPayInterceptor;

@SuppressWarnings("deprecation")
public abstract class PayWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

	@Bean
	CharacterEncodInterceptor characterEncodInterceptor() {
		return new CharacterEncodInterceptor();
	}
	
	@Bean
	AliPayInterceptor aliPayInterceptor() {
		return new AliPayInterceptor();
	}
	
	@Bean
	WxPayInterceptor wxPayInterceptor() {
		return new WxPayInterceptor();
	}
	
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	registry.addInterceptor(characterEncodInterceptor()).addPathPatterns(unionPayUrl());
        registry.addInterceptor(aliPayInterceptor()).addPathPatterns(aliPayUrl());
        registry.addInterceptor(wxPayInterceptor()).addPathPatterns(wxPayUrl());
        super.addInterceptors(registry);
    }
    
    /**获取wx支付时需要拦截的控制器
     * @return
    */
    protected abstract List<String> wxPayUrl();
    
    
    /**获取ali支付时需要拦截的控制器
     * @return
    */
    protected abstract List<String> aliPayUrl();
    
    /**银联支付
     * @return
    */
    protected abstract List<String> unionPayUrl();
    
    
}

