package com.org.lg.bean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class PayConfig {
	
	@Component
	@ConfigurationProperties(prefix = "pay.alipay")
	public static class AliPay {
		
		public static String   appId;
		public static String   privateKey;
		public static String   publicKey;
		public static String   signType;
		public static String   alipayPublicKey;
		public static String   domain;
		public static String   serviceUrl;

		public void setAppId(String appId) {
			PayConfig.AliPay.appId = appId;
		}

		public  void setPrivateKey(String privateKey) {
			AliPay.privateKey = privateKey;
		}

		public  void setPublicKey(String publicKey) {
			AliPay.publicKey = publicKey;
		}

		public  void setSignType(String signType) {
			AliPay.signType = signType;
		}

		public  void setDomain(String domain) {
			AliPay.domain = domain;
		}

		public  void setAlipayPublicKey(String alipayPublicKey) {
			AliPay.alipayPublicKey = alipayPublicKey;
		}

		public static void setServiceUrl(String serviceUrl) {
			AliPay.serviceUrl = serviceUrl;
		}
		
	}
	
	@Component("wxpay")
	@ConfigurationProperties(prefix = "pay.wxpay")
	public static class WxPay{
		
		public static String appId;
		
		
		public static String appSecret;
		
		
		public static String mchId;
		
		
		public static String partnerKey;
		
		
		public static String certPath;
		
		
		public static String domain;

		public  void setAppId(String appId) {
			PayConfig.WxPay.appId = appId;
		}

		public  void setAppSecret(String appSecret) {
			PayConfig.WxPay.appSecret = appSecret;
		}

		public  void setMchId(String mchId) {
			PayConfig.WxPay.mchId = mchId;
		}

		public  void setPartnerKey(String partnerKey) {
			WxPay.partnerKey = partnerKey;
		}

		public  void setCertPath(String certPath) {
			PayConfig.WxPay.certPath = certPath;
		}

		public  void setDomain(String domain) {
			PayConfig.WxPay.domain = domain;
		}
		
	}
}
