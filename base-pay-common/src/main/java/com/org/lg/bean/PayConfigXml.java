package com.org.lg.bean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jpay.alipay.AliPayApiConfig;
import com.jpay.weixin.api.WxPayApiConfig;
import com.jpay.weixin.api.WxPayApiConfig.PayModel;

@Configuration
public class PayConfigXml {

	@Bean
	AliPayApiConfig aliPayApiConfig() {
		return AliPayApiConfig.New()
				.setAppId(PayConfig.AliPay.appId)
				.setAlipayPublicKey(PayConfig.AliPay.alipayPublicKey)
				.setCharset("UTF-8")
				.setPrivateKey(PayConfig.AliPay.privateKey)
				.setServiceUrl(PayConfig.AliPay.serviceUrl)
				.setSignType(PayConfig.AliPay.signType)
				.build();
	}
	
	@Bean
	public WxPayApiConfig wxPayApiConfig() {
		return WxPayApiConfig.New()
				.setAppId(PayConfig.WxPay.appId)
				.setMchId(PayConfig.WxPay.mchId)
				.setPaternerKey(PayConfig.WxPay.partnerKey)
				.setPayModel(PayModel.BUSINESSMODEL)
				.setNotifyUrl(PayConfig.WxPay.domain+"/wxpay/pay_notify");
	}
}
