package com.org.lg.interceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import com.jpay.alipay.AliPayApiConfig;
import com.jpay.alipay.AliPayApiConfigKit;
import com.org.lg.base.AliNotifyController;

public class AliPayInterceptor implements HandlerInterceptor {

	@Autowired
	AliPayApiConfig aliPayApiConfig;
	
	@Override
	public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object handler) throws Exception {
		AliPayApiConfigKit.setThreadLocalAliPayApiConfig(aliPayApiConfig);
		return true;
		/*if (HandlerMethod.class.equals(handler.getClass())) {
			HandlerMethod method = (HandlerMethod) handler;
			Object controller = method.getBean();
			if (controller instanceof AliNotifyController == false) {
				throw new RuntimeException("请保证控制器需要继承 AliNotifyController");
			}
			try {
				AliPayApiConfigKit.setThreadLocalAliPayApiConfig(aliPayApiConfig);
				return true;
			}
			finally {
			}
		}
		return false;*/
	}

	@Override
	public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Object o, Exception e) throws Exception {
	}
}
