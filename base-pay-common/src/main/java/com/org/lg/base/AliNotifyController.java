package com.org.lg.base;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.jpay.alipay.AliPayApi;
import com.org.lg.bean.PayConfig;
import com.org.lg.constant.Constant;

import lombok.extern.java.Log;

/**
 * @author LG
 * @version 1.0.0
 * @date 2018-10-31 10:26
 */
@Log
@RestController
public abstract class AliNotifyController {
	
	
	@RequestMapping(Constant.NOTIFY_ALI_NOTIFY_URL)
	public abstract String notifyUrl(HttpServletRequest request);
	
	
	@RequestMapping(Constant.NOTIFY_ALI_RETURN_URL)
	public abstract String returnUrl(HttpServletRequest request);
	
	
	protected boolean rsaCheckV1(HttpServletRequest request) throws AlipayApiException {
		Map<String, String> map = AliPayApi.toMap(request);
		return AlipaySignature.rsaCheckV1(map, PayConfig.AliPay.alipayPublicKey, "UTF-8",PayConfig.AliPay.signType);
	};
	
	protected String getParamByKey(HttpServletRequest request,String key) {
		String res="";
		Map<String, String> map = AliPayApi.toMap(request);
		for (Map.Entry<String, String> entry : map.entrySet()) {
			log.info(entry.getKey() + " = " + entry.getValue());
			if(key.equals(entry.getKey())) {
				res=entry.getValue();
				break;
			}
		}
		return res;
	}
 
}
