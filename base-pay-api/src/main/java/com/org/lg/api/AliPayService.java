package com.org.lg.api;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alipay.api.AlipayApiException;
import com.org.lg.model.TradePagePayModel;
import com.org.lg.model.TradeRefundModel;
import com.org.lg.model.TradeWapPayModel;
public interface AliPayService  {

	/**pc支付
	 * @param param
	 * @return
	 */
	void pcPay(HttpServletRequest request,HttpServletResponse response,TradePagePayModel payModel);
	
	
	/**wap 支付 （唤醒 app客户端支付）
	 * @param response
	 * @param payModel
	 */
	void wapPay(HttpServletResponse response,TradeWapPayModel payModel) throws AlipayApiException;

	
	/**校验支付宝通知是否合法
	 * @param map
	 * @return
	 */
	boolean rsaCheckV1(Map<String, String> map) throws AlipayApiException;
	
	
	/**阿里支付宝申请退款
	 * @param tradeRefundModel
	 * @return
	 */
	String tradeRefund(TradeRefundModel tradeRefundModel) throws AlipayApiException;
}
