package com.org.lg.api;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.org.lg.model.H5PayModel;
import com.org.lg.model.WechatPayModel;
import com.org.lg.model.WxApiResult;
import com.org.lg.model.WxTradeRefundModel;

public interface WxPayService  {

	/**微信扫码支付之 扫码获取支付二维码 （扫码支付模式二）
	 * @param param
	 * @return
	 */
	WxApiResult scanPay(Map<String, String> param);

	/**校验回调请求是否来自微信
	 * @param params
	 * @param paternerKey
	 * @return
	 */
	boolean verifyNotify(Map<String, String>params);
	
	
    /**h5 支付请求唤醒微信app
     * @param request
     * @param response
     */
	WxApiResult h5Pay(HttpServletResponse response,H5PayModel h5PayModel);
	
	
	/**
	 * 微信公众号支付
	 * @param param
	 * @return
	 */
	WxApiResult wechatPay(WechatPayModel wechatPayModel);
	
	
	/**退款申请
	 * @param params
	 * @return
	 */
	WxApiResult tradeRefund(WxTradeRefundModel tradeRefundModel );
}
